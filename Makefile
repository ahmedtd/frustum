#
# Build system for the frustum math library.
#

# We don't use any GNU Make built-in rules.
.SUFFIXES:
.ONESHELL:
.DELETE_ON_ERROR:

.PHONY: install
install:
	mkdir -p "$(DESTDIR)/usr/include/"
	rsync --recursive --chmod a=r,u=rw include/ "$(DESTDIR)/usr/include/"
